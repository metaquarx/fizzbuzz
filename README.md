# fizzbuzz

Simple fizzbuzz program written in C++

https://en.wikipedia.org/wiki/Fizz_buzz

# Building:

```
make
make clean
```

# Usage:

```
./fizzbuzz
```

Will run the program with the default count of 15. You can select your own count by passing it as the first argument:

```
./fizzbuzz 50
```