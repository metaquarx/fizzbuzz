# Copyright (C) 2020, metaquarx

# This file is part of fizzbuzz.
# 
# fizzbuzz is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# fizzbuzz is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with fizzbuzz.  If not, see <https://www.gnu.org/licenses/>.

CC := g++
CFLAGS := -c -I./src -std=c++17
ifeq ($(BUILD),debug)
	CFLAGS += -O0 -g -Wall -Wextra
	LOC = build/debug
else
	CFLAGS += -Ofast -s -DNDEBUG -march=native -mtune=native
	LOC = build/release
endif

.DEFAULT_GOAL := all
.PHONY: clean remove Binary all

clean:
	rm -rf build/

remove: clean
	rm -rf fizzbuzz

all:
	@mkdir -p $(LOC)
	@make --no-print-directory Binary

Binary: fizzbuzz
	@: # Remove "Nothing can be done" message

fizzbuzz: $(LOC)/main.o
	$(CC) $^ $(LDFLAGS) -o $@

$(LOC)/main.o: src/main.cpp src/main.hpp
	$(CC) $< $(CFLAGS) -o $@

