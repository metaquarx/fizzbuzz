// Copyright (C) 2020, metaquarx

// This file is part of fizzbuzz.
// 
// fizzbuzz is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// fizzbuzz is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with fizzbuzz.  If not, see <https://www.gnu.org/licenses/>.

#ifndef fizzbuzz_main_hpp
#define fizzbuzz_main_hpp

int main(int argc, char * argv[]);

#endif // fizzbuzz_main_hpp