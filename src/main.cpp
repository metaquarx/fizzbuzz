// Copyright (C) 2020, metaquarx

// This file is part of fizzbuzz.
// 
// fizzbuzz is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// fizzbuzz is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with fizzbuzz.  If not, see <https://www.gnu.org/licenses/>.

#include "main.hpp"

#include <cstddef>
#include <string_view>
#include <string>
#include <stdexcept>
#include <iostream>

void fizzbuzz(unsigned long count) {
	for (unsigned long i = 1; i < count + 1; i++) {
		bool mult = false;

		if (i % 3 == 0) {
			std::cout << "fizz";
			mult = true;
		}

		if (i % 5 == 0) {
			std::cout << "buzz";
			mult = true;
		}

		if (!mult) {
			std::cout << i;
		}

		std::cout << std::endl;
	}
}

int main(int argc, char * argv[]) {
	unsigned long count = 15;

	for (int i = 1; i < argc; i++) {
		try {
			count = stoul(std::string(argv[i]));
			break;
		} catch (std::invalid_argument & e) {
			std::cout << "Usage: " << argv[0] << " [count = 15]" << std::endl;
			return 0;
		}
	}

	fizzbuzz(count);

	return 0;
}